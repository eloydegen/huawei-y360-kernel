/*
 *
 * Copyright 2006, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Old implementation for phone_number_compare(), which has used in cupcake, but once replaced with
// the new, more strict version, and reverted again.

#include <string.h>
#include <utils/Log.h>
#include <android/log.h>

#ifdef WT_HUAWEI_GLOBAL
#include <cutils/properties.h>
#include <stdlib.h>
#endif

#ifdef MTK_CTA_DFO_SUPPORT
#include <DfoDefines.h>
#endif

// Simple logging macros.
#define LOG_TAG "OldPhoneNumberUtils"
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

namespace android {

#if defined(WT_HUAWEI_S98360_U23B1)
const int intMccMnc[] = {7,8,10};
const char* strMccMnc[] = {
                                  "72207,33403,334030,71606",
                                  "73002,74807,70403,704030,71030,710300,714020,71402,74000,71204",
                                  "732123,73404",
                              };

#endif
static int saveMatchCount = 0;

static int primarySlotIdMatch = -1;

static int getMatchCountryByMccMnc(){

    int matchCount = 7;

	char primarySlotId[100] ;
    memset(primarySlotId, 0, sizeof(char) * 100);
    
    char pMccMnc[100];// = (char *)malloc(100);
    memset(pMccMnc, 0, sizeof(char) * 100);
    
    property_get("gsm.sim.operator.numeric", pMccMnc, "");
    
    char strIsVirtual[100];// =  (char *)malloc(100);
    memset(strIsVirtual, 0, sizeof(char) * 100);

		
#ifdef WT_HUAWEI_S98360_U23B1
	if (strcmp(pMccMnc, "") == 0 || strcmp(pMccMnc, "0") == 0)
	{
		//do nothing 
	}
	else
	{
		for(int i = 0; i < sizeof(strMccMnc) / sizeof(strMccMnc[0]); i++)
			if(strstr(strMccMnc[i], pMccMnc) != NULL)
				{
					matchCount = intMccMnc[i];
					//free(pMccMnc);
					//free(strIsVirtual);
					return matchCount;
				}

	}
#endif
   
	property_get("gsm.primary.sim.slot", primarySlotId, "");

	if (strcmp(primarySlotId, "") == 0){
		return matchCount;
	}else if(strcmp(primarySlotId, "-1") == 0){
		return matchCount;
	}else{
		if(saveMatchCount >0)
		   {
			   //LOGD("OldPhoneNumberUtils000.cpp, saveMatchCount=%d", saveMatchCount);
			   if(primarySlotIdMatch == atoi(primarySlotId)){
				   return saveMatchCount;
			   } 
		   }
		primarySlotIdMatch = atoi(primarySlotId);
	}
    
    LOGD("OldPhoneNumberUtils.cpp, primarySlotId=%d", primarySlotIdMatch);

	if(primarySlotIdMatch == 0){
		{
        property_get("gsm.sim.operator.numeric", pMccMnc, "");
        property_get("ril.virtual.num_match",strIsVirtual,"");
			        if (strcmp(strIsVirtual, "") == 0 || strcmp(strIsVirtual, "0") == 0){
                        property_get("ril.global.num_match",strIsVirtual,"");    
			        }else{
			            property_get("ril.virtual.num_match",strIsVirtual,"");
			        }
                    
           // free(pMccMnc);

            if ((strcmp(strIsVirtual, "") == 0)){
                //free(strIsVirtual);
                return matchCount;
            }else{
				char  *pos =0;
				if((pos = strstr(strIsVirtual,",")) != 0 )
				{
					char  matchCount1[100];// =	(char *)malloc(100);
					char   matchCount2[100];// =	(char *)malloc(100);
					memset(matchCount1, 0, sizeof(char) * 100);
					memset(matchCount2, 0, sizeof(char) * 100);
					int  strAlllen = strlen(strIsVirtual);
					strncpy(matchCount1,strIsVirtual,strAlllen-strlen(pos) );
					strncpy(matchCount2,pos+1,strlen(pos) -1 );

					matchCount = atoi(matchCount1) >atoi(matchCount2) ? atoi(matchCount2):atoi(matchCount1);
					saveMatchCount = matchCount;
					//free(matchCount1);
					//free(matchCount2);
				}
				else
					{
						matchCount = atoi(strIsVirtual);
						saveMatchCount = matchCount;
					}
				 LOGD("OldPhoneNumberUtils111.cpp, matchCount=%d", matchCount);
                //free(strIsVirtual);
                return matchCount;
            }
            
    }

	}else if(primarySlotIdMatch == 1){

		//if (strcmp(pMccMnc, "") == 0 || strcmp(pMccMnc, "0") == 0)
    	{
        memset(pMccMnc, 0, sizeof(char) * 100);
        memset(strIsVirtual, 0, sizeof(char) * 100);
        property_get("gsm.sim.operator.numeric.2", pMccMnc, ""); 
        if (strcmp(pMccMnc, "") == 0 || strcmp(pMccMnc, "0") == 0)
        {
           // free(pMccMnc);
            //free(strIsVirtual);
            LOGD("OldPhoneNumberUtils000.cpp, matchCount=%d", atoi(strIsVirtual));
            return matchCount;
        }else{
            property_get("gsm.sim.operator.numeric.2", pMccMnc, "");
            property_get("ril.virtual.num_match.2",strIsVirtual,"");
			        if (strcmp(strIsVirtual, "") == 0 || strcmp(strIsVirtual, "0") == 0){
                        property_get("ril.global.num_match.2",strIsVirtual,"");    
			        }else{
			            property_get("ril.virtual.num_match.2",strIsVirtual,"");
			        }

            //free(pMccMnc);

            if ((strcmp(strIsVirtual, "") == 0)){
                //free(strIsVirtual);
                return matchCount;
            }else{
				char  *pos =0;
				if((pos = strstr(strIsVirtual,",")) != 0 )
				{
					char matchCount1[100];// =	(char *)malloc(100);
					char   matchCount2[100];// =	(char *)malloc(100);
					memset(matchCount1, 0, sizeof(char) * 100);
					memset(matchCount2, 0, sizeof(char) * 100);
					int  strAlllen = strlen(strIsVirtual);
					strncpy(matchCount1,strIsVirtual,strAlllen-strlen(pos) );
					strncpy(matchCount2,pos+1,strlen(pos) -1 );

					matchCount = atoi(matchCount1) >atoi(matchCount2) ? atoi(matchCount2):atoi(matchCount1);
					saveMatchCount = matchCount ;
					LOGD("OldPhoneNumberUtils111.cpp, atoi matchCount=%d", matchCount);
					//free(matchCount1);
					//free(matchCount2);
				}
				else
					{
						matchCount = atoi(strIsVirtual);
						saveMatchCount = matchCount;
					}
				LOGD("OldPhoneNumberUtils222.cpp, matchCount=%d", matchCount);
               // free(strIsVirtual);
                return matchCount;
            }
    
        }
    }

	}//else if(primarySlotIdMatch == -1)
	else{
		saveMatchCount = matchCount;
		return matchCount;
	}

}


static int getMinMatch() 
{
#ifdef WT_HUAWEI_GLOBAL
    return getMatchCountryByMccMnc();
#elif defined(MTK_LONG_MIN_MATCH) || defined(MTK_OP09_SUPPORT)
    return 11;
#elif defined(MTK_CTA_DFO_SUPPORT)
    if (MTK_CTA_SUPPORT)
        return 11;
    else
        return 7;	
#endif
    return 7;
}

/** True if c is ISO-LATIN characters 0-9 */
static bool isISODigit (char c)
{
    return c >= '0' && c <= '9';
}

/** True if c is ISO-LATIN characters 0-9, *, # , +  */
static bool isNonSeparator(char c)
{
    return (c >= '0' && c <= '9') || c == '*' || c == '#' || c == '+';
}

/**
 * Phone numbers are stored in "lookup" form in the database
 * as reversed strings to allow for caller ID lookup
 *
 * This method takes a phone number and makes a valid SQL "LIKE"
 * string that will match the lookup form
 *
 */
/** all of a up to len must be an international prefix or
 *  separators/non-dialing digits
 */
static bool matchIntlPrefix(const char* a, int len)
{
    /* '([^0-9*#+]\+[^0-9*#+] | [^0-9*#+]0(0|11)[^0-9*#+] )$' */
    /*        0    1                     2 3 45               */

    int state = 0;
    for (int i = 0 ; i < len ; i++) {
        char c = a[i];

        switch (state) {
            case 0:
                if      (c == '+') state = 1;
                else if (c == '0') state = 2;
                else if (isNonSeparator(c)) return false;
            break;

            case 2:
                if      (c == '0') state = 3;
                else if (c == '1') state = 4;
                else if (isNonSeparator(c)) return false;
            break;

            case 4:
                if      (c == '1') state = 5;
                else if (isNonSeparator(c)) return false;
            break;

            default:
                if (isNonSeparator(c)) return false;
            break;

        }
    }

    return state == 1 || state == 3 || state == 5;
}

/** all of 'a' up to len must match non-US trunk prefix ('0') */
static bool matchTrunkPrefix(const char* a, int len)
{
    bool found;

    found = false;

    for (int i = 0 ; i < len ; i++) {
        char c = a[i];

        if (c == '0' && !found) {
            found = true;
        } else if (isNonSeparator(c)) {
            return false;
        }
    }

    return found;
}

/** all of 'a' up to len must be a (+|00|011)country code)
 *  We're fast and loose with the country code. Any \d{1,3} matches */
static bool matchIntlPrefixAndCC(const char* a, int len)
{
    /*  [^0-9*#+]*(\+|0(0|11)\d\d?\d? [^0-9*#+] $ */
    /*      0       1 2 3 45  6 7  8              */

    int state = 0;
    for (int i = 0 ; i < len ; i++ ) {
        char c = a[i];

        switch (state) {
            case 0:
                if      (c == '+') state = 1;
                else if (c == '0') state = 2;
                else if (isNonSeparator(c)) return false;
            break;

            case 2:
                if      (c == '0') state = 3;
                else if (c == '1') state = 4;
                else if (isNonSeparator(c)) return false;
            break;

            case 4:
                if      (c == '1') state = 5;
                else if (isNonSeparator(c)) return false;
            break;

            case 1:
            case 3:
            case 5:
                if      (isISODigit(c)) state = 6;
                else if (isNonSeparator(c)) return false;
            break;

            case 6:
            case 7:
                if      (isISODigit(c)) state++;
                else if (isNonSeparator(c)) return false;
            break;

            default:
                if (isNonSeparator(c)) return false;
        }
    }

    return state == 6 || state == 7 || state == 8;
}

/** or -1 if both are negative */
static int minPositive(int a, int b)
{
    if (a >= 0 && b >= 0) {
        return (a < b) ? a : b;
    } else if (a >= 0) { /* && b < 0 */
        return a;
    } else if (b >= 0) { /* && a < 0 */
        return b;
    } else { /* a < 0 && b < 0 */
        return -1;
    }
}

/**
 * Return the offset into a of the first appearance of b, or -1 if there
 * is no such character in a.
 */
static int indexOf(const char *a, char b) {
    const char *ix = strchr(a, b);

    if (ix == NULL)
        return -1;
    else
        return ix - a;
}

/**
 * Compare phone numbers a and b, return true if they're identical
 * enough for caller ID purposes.
 *
 * - Compares from right to left
 * - requires MIN_MATCH (7) characters to match
 * - handles common trunk prefixes and international prefixes
 *   (basically, everything except the Russian trunk prefix)
 *
 * Tolerates nulls
 */
bool phone_number_compare_loose(const char* a, const char* b)
{
    int ia, ib;
    int matched;
    int numSeparatorCharsInA = 0;
    int numSeparatorCharsInB = 0;

    if (a == NULL || b == NULL) {
        return false;
    }

    //LOGD("phone_number_compare_loose: a=%s, b=%s", a, b);

    ia = strlen(a);
    ib = strlen(b);
    if (ia == 0 || ib == 0) {
        return false;
    }

    // Compare from right to left
    ia--;
    ib--;

    matched = 0;

    while (ia >= 0 && ib >=0) {
        char ca, cb;
        bool skipCmp = false;

        ca = a[ia];

        if (!isNonSeparator(ca)) {
            ia--;
            skipCmp = true;
            numSeparatorCharsInA++;
        }

        cb = b[ib];

        if (!isNonSeparator(cb)) {
            ib--;
            skipCmp = true;
            numSeparatorCharsInB++;
        }

        if (!skipCmp) {
            if (cb != ca) {
                break;
            }
            ia--; ib--; matched++;
        }
    }

    //LOGD("phone_number_compare_loose: matched=%d, MIN_MATCH=%d", matched, getMinMatch());

    if (matched < getMinMatch()) {
        const int effectiveALen = strlen(a) - numSeparatorCharsInA;
        const int effectiveBLen = strlen(b) - numSeparatorCharsInB;

        //LOGD("phone_number_compare_loose: eALen=%d, eBLen=%d", effectiveALen, effectiveBLen);

        // if the number of dialable chars in a and b match, but the matched chars < MIN_MATCH,
        // treat them as equal (i.e. 404-04 and 40404)
        if (effectiveALen == effectiveBLen && effectiveALen == matched) {
            return true;
        }

        return false;
    }

    // At least one string has matched completely;
    if (matched >= getMinMatch()  && (ia < 0 || ib < 0)) {
        return true;
    }

    /*
     * Now, what remains must be one of the following for a
     * match:
     *
     *  - a '+' on one and a '00' or a '011' on the other
     *  - a '0' on one and a (+,00)<country code> on the other
     *     (for this, a '0' and a '00' prefix would have succeeded above)
     */

    if (matchIntlPrefix(a, ia + 1) && matchIntlPrefix(b, ib +1)) {
        return true;
    }

    if (matchTrunkPrefix(a, ia + 1) && matchIntlPrefixAndCC(b, ib +1)) {
        return true;
    }

    if (matchTrunkPrefix(b, ib + 1) && matchIntlPrefixAndCC(a, ia +1)) {
        return true;
    }

    /*
     * Last resort: if the number of unmatched characters on both sides is less than or equal
     * to the length of the longest country code and only one number starts with a + accept
     * the match. This is because some countries like France and Russia have an extra prefix
     * digit that is used when dialing locally in country that does not show up when you dial
     * the number using the country code. In France this prefix digit is used to determine
     * which land line carrier to route the call over.
     */
    /*bool aPlusFirst = (*a == '+');
    bool bPlusFirst = (*b == '+');
    if (ia < 4 && ib < 4 && (aPlusFirst || bPlusFirst) && !(aPlusFirst && bPlusFirst)) {
        return true;
    }*/

    //LOGD("phone_number_compare_loose: return false");

    return false;
}

}  // namespace android
